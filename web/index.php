<?php
/*
    DNSTest.org Resolver-Tester, a tool to test that your DNS queries are being
    resolved by the services and providers you intended.
    Copyright (C) 2016  Iceberg Technologies Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



$version = "0.2.1";
if(isset($_GET['reqnum'])){
	$reqNum = $_GET['reqnum'];
	$reqBase = "requests/";
	$results = array();
	if(file_exists($reqBase.$reqNum)){
		$file = file_get_contents($reqBase.$reqNum);
		$results = explode("\n", $file);
		if(sizeof($results) != 0){
			if(strlen($results[sizeof($results)-1]) == 0)
				unset($results[sizeof($results)-1]);
		}
	}
	echo json_encode($results);
	exit(0);
}?>
<head><style type="text/css">body{margin:40px auto;max-width:650px;line-height:1.6;font-size:18px;color:#444;padding:0 10px}h1,h2,h3{line-height:1.2}.error{color:red}.testing{font-size:.75em}pre{line-height:.5}.about{font-size:.75em}.resultarea{background-color:azure;padding:20px;}</style><meta content="text/html;charset=utf-8" http-equiv="Content-Type"><meta content="utf-8" http-equiv="encoding"></head>
<?php
$uniqid =  md5($_SERVER["REMOTE_ADDR"].time().uniqid());
$testDomains = array("OpenNIC TLD"=>".tester.chip.geek","ICANN TLD"=>".tester.nerds.io","ICANN TLD 2"=>".test.dnstest.org");
$imageTestHTML = "<img src='http://$uniqid".$testDomains['OpenNIC TLD']."/opennicblock.gif' /><br/>If you can see a green box, you can resolve OpenNIC TLDs.<br/><img src='http://$uniqid".$testDomains['ICANN TLD']."/icannblock.gif' /><br/>If you can see a blue box, you can resolve ICANN TLDs.";
$aboutFooter = "<span class='about'><br/><br/>This is a project by chip and is <a href='https://bitbucket.org/chpwssn/resolver-tester'>open source</a>. Version: $version</span>";
	
if (!isset($_GET['advanced'])) {
	echo $imageTestHTML."<br/><br/>Having trouble? <a href='./?advanced'>Try the advanced test.</a>$aboutFooter";
	exit(0);
}
if(isset($_GET['advanced'])){
	if($_SERVER["SERVER_NAME"] == "tester.chip.geek" || $_SERVER["SERVER_NAME"] == "tester.nerds.io" || $_SERVER["SERVER_NAME"] == "test.dnstest.org"){
	
	?>
	<script src="jquery-2.2.0.min.js"></script>
	<script>
	var debug = false;
	$.globals = {
		fetchcount : 0,
		t2s : []
	};
	$(function () {
		getT2List();
		//Reset to 0 fetchcount to resume fetching the resolvers list
		$("#continuescan").click(function() {
			$.globals.fetchcount = 0;
			$("#status").attr("hidden", true);
		});
		var timeout = 5000;
		var action = function() {
		    fetch($.globals.t2s);
		};
		setInterval(action, timeout);
	});
	function getT2List(){
		$.ajax({
			url: "http://<?=$_SERVER['SERVER_NAME'];?>/servmap.php",
			}).done(function(data){
				$("#loading").attr("hidden",true);
				var obj = $.parseJSON(data);
				$.globals.t2s = obj;
		}).error(function(){
			$("#t2error").attr("hidden",false);
		});
	}
	function fetch(T2list){
		//  Don't do any more AJAX requests after 100, prevents excess bandwidth use if the user
		//  leaves the tab open
		if ($.globals.fetchcount > 100) {
			$("#status").attr("hidden", false);
			return;
		}
		$.ajax({
			url: "http://<?=$_SERVER['SERVER_NAME'];?>/?reqnum=<?=$uniqid;?>",
			}).done(function(data) {
				if(debug) console.log(data);
				var obj = jQuery.parseJSON(data);
				$("#resolvers").html("");
				if(debug) console.log(T2list);
				$.each(obj, function(id, item){
					var isT2 = "";
					if (T2list.length) {
						isT2 = "This does not appear to be an OpenNIC T2 Server";
						$.each($.globals.t2s, function(t2i,t2item){
							if (t2item.v4 == item) {
								isT2 = "This is an OpenNIC T2 Server. <a target='_blank' href='http://report.opennicproject.org/t2log/t2.php?ip_addr="+item+"'>Test it.</a> (Current state: "+t2item.stateString+")";
								if (t2item.state != "norm") {
									isT2 += " <span class='error'>This is not a good thing, you should keep an eye on this.</span>";
								}
							}
						});
					}
					$("#resolvers").html(
						$("#resolvers").html() + "<b>" + item+"</b> "+isT2+"<br/>"
					);
					if(debug) console.log(item);
				});
		});
		$.globals.fetchcount++;
	}
	</script>
	<?php
		//var_dump($_SERVER);
		echo "<h2>Method 1: Load images</h2>$imageTestHTML";
		// Method 2
		echo" <h2>Method 2: Load webpage</h2>Try and visit one of the following pages:<ul>";
		foreach ($testDomains as $domain) {
			echo "<li><a href='http://$uniqid$domain' target='_blank'>$uniqid$domain</a></li>";
		}
		echo "</ul>";
		//Method 3
		echo "<h2>Method 3: Command line DNS client</h2>Run the following commands to perform the lookup:<br/>*nix:<br/>";
		foreach ($testDomains as $type => $domain) {
			echo "<pre>dig $uniqid$domain</pre>";
		}
		echo "<br/>Windows:<br/>";
		foreach ($testDomains as $type => $domain) {
			echo "<pre>nslookup $uniqid$domain</pre>";
		}
		//Results
		echo "<br/>Resolvers that have queried $uniqid:<br/><span id='status' hidden>We've refreshed 100 times? <button id='continuescan'>Continue?</button><br/></span><span class='error' id='t2error' hidden>Oops, we couldn't get the list of OpenNIC T2 Servers<br/></span><span id='loading'>Loading list of OpenNIC T2 Servers...<br/></span><div class='resultarea'><span id='resolvers'>(warming up)</span></div>";
		//DigBot example
		echo "<br/><br/><span class='testing'><br/>You can test this system by saying:<br/><pre>digbot: dig $uniqid".$testDomains['OpenNIC TLD']."</pre>in any IRC channel where digbot is present. You should see '<b>96.90.175.167</b> This is an OpenNIC T2 Server.' appear in your resolvers list.</span>$aboutFooter";
	}
}
