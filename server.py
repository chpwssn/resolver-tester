# -*- coding: utf-8 -*-

"""
    This DNS server will reply with the same response and log the client resolver to a
    file with the uniqid as its name. 

    DNSTest.org Resolver-Tester, a tool to test that your DNS queries are being
    resolved by the services and providers you intended.
    Copyright (C) 2016  Iceberg Technologies Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import print_function

import copy
import os

from dnslib import RR
from dnslib.server import DNSServer,DNSHandler,BaseResolver,DNSLogger

testerDNs = [".tester.chip.geek.",".tester.nerds.io.",".test.dnstest.org."]
requestDir = "/var/www/html/tester/requests/"	#For now make sure this ends in /

class FixedResolver(BaseResolver):
	"""
		Respond with fixed response to all requests
	"""
	def __init__(self,zone):
		# Parse RRs
		self.rrs = RR.fromZone(zone)

	def resolve(self,request,handler):
		reply = request.reply()
		qname = request.q.qname
		uid = str(qname)
		for dn in testerDNs:
			uid = uid.replace(dn,"")
		with open("{0}{1}".format(requestDir, uid), "a") as reqfile:
			print("Logging Request for {0}".format(uid))
			reqfile.write(handler.client_address[0]+"\n")
		# Replace labels with request label
		for rr in self.rrs:
			a = copy.copy(rr)
			a.rname = qname
			reply.add_answer(a)
		return reply

if __name__ == '__main__':

	import argparse,sys,time

	p = argparse.ArgumentParser(description="Fixed DNS Resolver")
	p.add_argument("--response","-r",default=". 60 IN A 127.0.0.1",
					metavar="<response>",
					help="DNS response (zone format) (default: 127.0.0.1)")
	p.add_argument("--zonefile","-f",
					metavar="<zonefile>",
					help="DNS response (zone file, '-' for stdin)")
	p.add_argument("--port","-p",type=int,default=53,
					metavar="<port>",
					help="Server port (default:53)")
	p.add_argument("--address","-a",default="",
					metavar="<address>",
					help="Listen address (default:all)")
	p.add_argument("--udplen","-u",type=int,default=0,
					metavar="<udplen>",
					help="Max UDP packet length (default:0)")
	p.add_argument("--tcp",action='store_true',default=False,
					help="TCP server (default: UDP only)")
	p.add_argument("--log",default="request,reply,truncated,error",
					help="Log hooks to enable (default: +request,+reply,+truncated,+error,-recv,-send,-data)")
	p.add_argument("--log-prefix",action='store_true',default=False,
					help="Log prefix (timestamp/handler/resolver) (default: False)")
	args = p.parse_args()
	
	if args.zonefile:
		if args.zonefile == '-':
			args.response = sys.stdin
		else:
			args.response = open(args.zonefile)

	resolver = FixedResolver(args.response)
	logger = DNSLogger(args.log,args.log_prefix)

	print("Starting Fixed Resolver (%s:%d) [%s]" % (
						args.address or "*",
						args.port,
						"UDP/TCP" if args.tcp else "UDP"))

	for rr in resolver.rrs:
		print("	| ",rr.toZone().strip(),sep="")
	print()

	if args.udplen:
		DNSHandler.udplen = args.udplen

	udp_server = DNSServer(resolver,
						   port=args.port,
						   address=args.address,
						   logger=logger)
	udp_server.start_thread()

	if args.tcp:
		tcp_server = DNSServer(resolver,
							   port=args.port,
							   address=args.address,
							   tcp=True,
							   logger=logger)
		tcp_server.start_thread()

	while udp_server.isAlive():
		time.sleep(1)

		

