#Resolver Tester
This is a project that is a work in progress. The general idea is to troubleshoot DNS resolver issues by giving the user multiple ways to run tests in an easy fashion.

You can view/try it at http://test.dnstest.org and http://tester.chip.geek

Advanced tests can be seen at http://test.dnstest.org/?advanced and http://tester.chip.geek/?advanced

#How it works
##Components
1) Web Server

2) dnslib based DNS server that provides a wildcard response, allowing the unique urls

##DNS Lookup Methods
1) Look up image with user-unique domain names when the user loads the page

2) Give the user links to web pages with the user-unique domain names

3) Give the user commands that they can run on their computer to run the DNS queries from the command line

#Set Up
##Requirements
* Python
* Python-pip
* dnslib
* irc Python lib
* PHP5

##Getting Started
1)Get code and requisites


	git clone https://bitbucket.org/chpwssn/resolver-tester.git
	cd resolver-tester
	pip install --user -r requirements.txt


2) Edit zonefile to reflect your test domains

3) Edit server.py and replace `testerDNs` and `requestDir` with your installation details

4) Launch DNS server in tmux or screen session with:


	sudo python server.py -f zonefile

5) Move the contents of `web/` to your webserver directory

6) Update `$testDomains` and `$reqBase` to reflect your installation details

